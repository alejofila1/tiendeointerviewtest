package com.tiendeo.interviewtest.browseShops.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.browseShops.fragment.ShopsListFragment
import com.tiendeo.interviewtest.browseShops.fragment.ShopsMapFragment

class ShopsPagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) ShopsListFragment() else ShopsMapFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return context.getString(R.string.title_list_tab)
            1 -> return context.getString(R.string.title_map_tab)
        }
        return null
    }
}