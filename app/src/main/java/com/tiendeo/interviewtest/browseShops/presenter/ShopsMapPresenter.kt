package com.tiendeo.interviewtest.browseShops.presenter


import com.example.domain.model.LatLong
import com.example.domain.usecases.GetShopMarkersUseCase
import com.google.android.gms.maps.model.LatLng
import com.tiendeo.interviewtest.common.ShopMapper
import com.tiendeo.interviewtest.common.presenter.BasePresenter
import com.tiendeo.interviewtest.common.uimodel.MarkerOptionWrapper
import io.reactivex.Scheduler

class ShopsMapPresenter(private val shopsMapView: ShopsMapView,
                        private val getShopMarkersUseCase: GetShopMarkersUseCase,
                        mainScheduler: Scheduler,
                        backgroundScheduler: Scheduler) : BasePresenter(mainScheduler, backgroundScheduler) {
    companion object {
         val TIENDEO = LatLng(41.380968, 2.185584)
    }
    fun getShopsMarkers() {
        disposableBag?.add(getShopMarkersUseCase()
                .map { pair ->
                    MarkerOptionWrapper(
                            shopUiModel = ShopMapper.fromDomainToUiModel(pair.shop),
                            latLong = (LatLong(pair.latLng.lat, pair.latLng.long)
                                    ))
                }
                .observeOn(mainScheduler)
                .subscribeOn(backgroundScheduler)


                .subscribe({ marker ->
                    shopsMapView.drawShopOnMap(marker)

                }) {
                    shopsMapView.showNoShopsError()
                })


    }
}

interface ShopsMapView {
    fun drawShopOnMap(markerOptions: MarkerOptionWrapper)
    fun showNoShopsError()
}
