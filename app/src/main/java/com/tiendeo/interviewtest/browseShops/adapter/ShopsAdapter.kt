package com.tiendeo.interviewtest.browseShops.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel
import java.util.ArrayList

internal class ShopsAdapter( val shopClickLitener: (ShopUiModel)-> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var shopUiModels: MutableList<ShopUiModel> = mutableListOf()
    set(value) {
        this.shopUiModels.addAll(value)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ShopViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_shop, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ShopViewHolder).bind(shopUiModels[position])
    }

    override fun getItemCount(): Int {
        return shopUiModels.size
    }

    internal inner class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var shopNameView: TextView
        var shopAddressView: TextView

        init {
            shopNameView = itemView.findViewById(R.id.shop_name_view) as TextView
            shopAddressView = itemView.findViewById(R.id.shop_address_view) as TextView
        }

        fun bind(shopUiModel: ShopUiModel) {
            shopNameView.text = shopUiModel.shopName
            shopAddressView.text = shopUiModel.addressStreet + " " + shopUiModel.addressCity + " " + shopUiModel.addressPostalCode
            itemView.setOnClickListener {
               shopClickLitener.invoke(shopUiModel)
            }
        }
    }

}