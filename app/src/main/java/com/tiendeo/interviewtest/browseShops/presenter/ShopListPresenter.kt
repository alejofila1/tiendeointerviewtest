package com.tiendeo.interviewtest.browseShops.presenter

import com.example.domain.usecases.GetShopsUseCase
import com.tiendeo.interviewtest.common.ShopMapper
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel
import com.tiendeo.interviewtest.common.presenter.BasePresenter
import io.reactivex.Scheduler

class ShopListPresenter(val view: ShopListView,
                        private val getShopsUseCase: GetShopsUseCase,
                        mainScheduler: Scheduler,
                        backgroundScheduler: Scheduler) : BasePresenter(mainScheduler,backgroundScheduler) {


    fun getShopList() {
        disposableBag?.add(getShopsUseCase()
                .flattenAsObservable { it }
                .map { shop-> ShopMapper.fromDomainToUiModel(shop) }
                .toList()
                .map { it.toMutableList() }
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe { shops ->
                    if(shops.isNotEmpty()){
                        view.showShopList(shops)
                    }
                    else{
                        view.showNoShopMessage()
                    }
                })
    }
}

interface ShopListView {
    fun showShopList(list: MutableList<ShopUiModel>)
    fun showNoShopMessage()
}

