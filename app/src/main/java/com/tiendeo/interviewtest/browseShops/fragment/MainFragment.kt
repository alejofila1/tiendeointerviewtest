package com.tiendeo.interviewtest.browseShops.fragment

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.browseShops.adapter.ShopsPagerAdapter

class MainFragment : Fragment() {

    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: ShopsPagerAdapter
    private lateinit var tabLayout: TabLayout
    private lateinit var toolbar: Toolbar


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater?.inflate(R.layout.fragment_main, container, false)

        viewPager = rootView?.findViewById(R.id.pager) as ViewPager
        toolbar = rootView.findViewById(R.id.toolbar) as Toolbar

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        tabLayout = rootView.findViewById(R.id.tabs) as TabLayout

        pagerAdapter = ShopsPagerAdapter(childFragmentManager, context)
        tabLayout.setupWithViewPager(viewPager)
        viewPager.adapter = pagerAdapter

        return rootView
    }

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }
}