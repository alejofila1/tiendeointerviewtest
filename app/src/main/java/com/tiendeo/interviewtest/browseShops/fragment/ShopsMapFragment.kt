package com.tiendeo.interviewtest.browseShops.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.domain.usecases.GetShopMarkersUseCase
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.browseShops.presenter.ShopsMapPresenter
import com.tiendeo.interviewtest.browseShops.presenter.ShopsMapView
import com.tiendeo.interviewtest.common.activity.FragmentNavigationCallback
import com.tiendeo.interviewtest.common.activity.ShopsActivity
import com.tiendeo.interviewtest.common.dependency.ShopRepositoryFactory
import com.tiendeo.interviewtest.common.uimodel.MarkerOptionWrapper
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel
import com.tiendeo.interviewtest.shopdetail.fragment.ShopDetailFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ShopsMapFragment : SupportMapFragment(), OnMapReadyCallback,
        ShopsMapView {

    private var map: GoogleMap? = null
    private lateinit var shopMapsPresenter: ShopsMapPresenter
    private lateinit var fragmentNavigationCallback: FragmentNavigationCallback
    private lateinit var rootView: View

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentNavigationCallback = context as FragmentNavigationCallback

    }


    override fun onCreateView(layoutInflater: LayoutInflater?, container: ViewGroup?, bundle: Bundle?): View? {
        rootView = super.onCreateView(layoutInflater, container, bundle)!!
        val shopRepository = ShopRepositoryFactory.getShopRepository()
        shopMapsPresenter = ShopsMapPresenter(this, GetShopMarkersUseCase(shopRepository), AndroidSchedulers.mainThread(), Schedulers.io())
        shopMapsPresenter.onStart()
        getMapAsync(this)
        return rootView
    }

    override fun showNoShopsError() {
        Snackbar.make(rootView,R.string.no_shop_error,Snackbar.LENGTH_SHORT)
    }


    override fun drawShopOnMap(markerOptions: MarkerOptionWrapper) {
        map?.addMarker(markerOptions.getMarker())?.tag = markerOptions.shopUiModel

        map?.setOnInfoWindowClickListener {
            val fragment = ShopDetailFragment.newInstance(it.tag as ShopUiModel)
            fragmentNavigationCallback.navigateTo(fragment, true)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map?.addMarker(MarkerOptions().position(ShopsMapPresenter.TIENDEO).title("Tiendeo Office"))
        map?.moveCamera(CameraUpdateFactory.newLatLngZoom(ShopsMapPresenter.TIENDEO, 16f))
        shopMapsPresenter.getShopsMarkers()

    }

    override fun onStop() {
        super.onStop()
        shopMapsPresenter.onStop()
    }

}