package com.tiendeo.interviewtest.browseShops.fragment

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.domain.usecases.GetShopsUseCase
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.browseShops.adapter.ShopsAdapter
import com.tiendeo.interviewtest.browseShops.presenter.ShopListPresenter
import com.tiendeo.interviewtest.browseShops.presenter.ShopListView
import com.tiendeo.interviewtest.common.activity.FragmentNavigationCallback
import com.tiendeo.interviewtest.common.dependency.ShopRepositoryFactory
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel
import com.tiendeo.interviewtest.shopdetail.fragment.ShopDetailFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ShopsListFragment : Fragment(), ShopListView {



    private lateinit var shopsView: RecyclerView
    private lateinit var adapter: ShopsAdapter
    lateinit var fragmentNavigationCallback: FragmentNavigationCallback
    lateinit var shopListPresenter: ShopListPresenter

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fragmentNavigationCallback = context as FragmentNavigationCallback

    }

    override fun showShopList(list: MutableList<ShopUiModel>) {
        adapter.shopUiModels = list
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_shops_list, container, false)
        val shopRepository = ShopRepositoryFactory.getShopRepository()
        // This could be done with a dependency injection tool like dagger
        shopListPresenter = ShopListPresenter(this, GetShopsUseCase(shopRepository),
                AndroidSchedulers.mainThread(), Schedulers.io())
        shopsView = view?.findViewById(R.id.shops_view) as RecyclerView
        adapter = ShopsAdapter { shopClicked ->
            val fragment = ShopDetailFragment.newInstance(shopClicked)
            fragmentNavigationCallback.navigateTo(fragment,true)
        }
        shopListPresenter.onStart()
        shopsView.adapter = adapter
        shopsView.layoutManager = LinearLayoutManager(activity)
        shopListPresenter.getShopList()
        return view
    }

    override fun showNoShopMessage() {
        Toast.makeText(context,getString(R.string.no_shop_error),Toast.LENGTH_SHORT)
                .show()
    }


    override fun onStop() {
        super.onStop()
        shopListPresenter.onStop()

    }
}