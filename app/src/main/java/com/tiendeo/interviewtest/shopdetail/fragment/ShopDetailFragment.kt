package com.tiendeo.interviewtest.shopdetail.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel

class ShopDetailFragment : Fragment(){
    lateinit var toolbar: Toolbar
    lateinit var shopNameView : TextView
    lateinit var shopAddressView: TextView
    lateinit var shopCityView: TextView
    lateinit var shopPhoneNumberView: TextView
    lateinit var shopWebSiteView: TextView
    lateinit var shopUiModel: ShopUiModel


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater?.inflate(R.layout.shop_detai_fragment, container, false)
        toolbar = rootView?.findViewById(R.id.toolbar) as Toolbar
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        shopNameView = rootView?.findViewById(R.id.shop_name) as TextView
        shopAddressView = rootView.findViewById(R.id.shop_address) as TextView
        shopCityView = rootView.findViewById(R.id.shop_city) as TextView

        shopPhoneNumberView = rootView.findViewById(R.id.shop_phone_number)
        shopWebSiteView = rootView.findViewById(R.id.shop_web_site)
        shopUiModel = arguments.getParcelable(KEY_SHOP)

        shopNameView.text = shopUiModel.shopName
        shopAddressView.text = shopUiModel.addressStreet
        shopCityView.text = shopUiModel.addressCity
        shopCityView.text = if (shopUiModel.web.isBlank()) getString(R.string.no_website_shop) else shopUiModel.web
        shopPhoneNumberView.text = shopUiModel.phoneNumber
        return rootView
    }


    companion object {
        private const val KEY_SHOP ="shop"
        fun newInstance(shopUiModel: ShopUiModel): ShopDetailFragment {
            val params = Bundle()
            params.putParcelable(KEY_SHOP,shopUiModel)
            val fragment = ShopDetailFragment()
            fragment.arguments = params
            return fragment
        }
    }
}