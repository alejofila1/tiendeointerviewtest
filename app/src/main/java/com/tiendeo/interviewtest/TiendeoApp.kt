package com.tiendeo.interviewtest

import android.app.Application
import android.content.Context

class TiendeoApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: TiendeoApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }


}