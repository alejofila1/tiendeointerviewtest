package com.tiendeo.interviewtest.common.dependency

import com.example.domain.ShopRepository
import com.tiendeo.interviewtest.TiendeoApp
import com.tiendeotest.data.api.ShopsService
import com.tiendeotest.data.db.AppDatabase
import com.tiendeotest.data.memory.ShopInMemory
import com.tiendeotest.data.repository.ShopRepositoryImpl
import com.tiendeotest.data.utils.ConnectivityManagerWrapperImpl

/**
 * This was a used as a dependency injection container, it should be done with
 * a tool like dagger
 */

object ShopRepositoryFactory {
    var shopsApi =    ShopsService.getShopsApi()
    var shopInMemory = ShopInMemory()
    var shopDao = AppDatabase.getInstance(TiendeoApp.applicationContext()).shopDao()

    fun getShopRepository(): ShopRepository {
        return ShopRepositoryImpl(
               shopInMemory,
                shopsApi,
                shopDao,
                ConnectivityManagerWrapperImpl(TiendeoApp.applicationContext()))
    }
}


