package com.tiendeo.interviewtest.common.uimodel

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by juanlu on 3/8/16.
 */
class ShopUiModel(
        var addressCity: String,
        var addressPostalCode: String,
        var addressStreet: String,
        var isHasCatalogs: Boolean = false,
        var latitude: String,
        var longitude: String,
        var phoneNumber: String,
        var retailerId: String,
        var retailerName: String,
        var shopId: String,
        var shopName: String,
        var web: String) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(addressCity)
        parcel.writeString(addressPostalCode)
        parcel.writeString(addressStreet)
        parcel.writeByte(if (isHasCatalogs) 1 else 0)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(phoneNumber)
        parcel.writeString(retailerId)
        parcel.writeString(retailerName)
        parcel.writeString(shopId)
        parcel.writeString(shopName)
        parcel.writeString(web)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ShopUiModel> {
        override fun createFromParcel(parcel: Parcel): ShopUiModel {
            return ShopUiModel(parcel)
        }

        override fun newArray(size: Int): Array<ShopUiModel?> {
            return arrayOfNulls(size)
        }
    }

}
