package com.tiendeo.interviewtest.common

import com.example.domain.model.Shop
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel

object ShopMapper{
    fun fromDomainToUiModel(shop: Shop) : ShopUiModel{
        with(shop){
            return ShopUiModel(addressCity,
                    addressPostalCode,
                    addressStreet,
                    isHasCatalogs,
                    latitude,
                    longitude,
                    phoneNumber,
                    retailerId,
                    retailerName,
                    shopId,
                    shopName,
                    web
                    )

        }
    }
}