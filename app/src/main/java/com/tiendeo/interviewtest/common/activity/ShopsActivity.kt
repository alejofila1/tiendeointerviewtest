package com.tiendeo.interviewtest.common.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.FrameLayout
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.tiendeo.interviewtest.R
import com.tiendeo.interviewtest.browseShops.adapter.ShopsPagerAdapter
import com.tiendeo.interviewtest.browseShops.fragment.MainFragment

class ShopsActivity : AppCompatActivity(), FragmentNavigationCallback {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shops)
        //MapsInitializer.initialize(this)
        if(supportFragmentManager.findFragmentById(R.id.content) == null){
            navigateTo(MainFragment.newInstance(),false)
        }


    }

    override fun navigateTo(fragment: Fragment, addToBackStack: Boolean) {
        if(addToBackStack) {
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left , android.R.anim.slide_out_right)
                    .replace(R.id.content, fragment)
                    .addToBackStack(null)
                    .commit()
        }
        else{
            supportFragmentManager.beginTransaction()
                    .add(R.id.content, fragment)
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .commit()
        }
    }




}

interface FragmentNavigationCallback {
    fun navigateTo(fragment: Fragment, addToBackStack: Boolean)
}
