package com.tiendeo.interviewtest.common.uimodel

import com.example.domain.model.LatLong
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

open class MarkerOptionWrapper(val shopUiModel: ShopUiModel, val latLong: LatLong){
    fun getMarker() : MarkerOptions{
        return MarkerOptions()
                .position(LatLng(latLong.lat,latLong.long))
                .title(shopUiModel.shopName)
                .snippet(shopUiModel.addressStreet)

    }
}