package com.tiendeo.interviewtest.browseShops.presenter

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import com.example.domain.usecases.GetShopMarkersUseCase
import com.tiendeo.interviewtest.browseShops.anything
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class ShopsMapPresenterTest {

    @Mock
    lateinit var view: ShopsMapView
    @Mock
    lateinit var repository: ShopRepository
    private val testScheduler = TestScheduler()
    lateinit var presenter: ShopsMapPresenter

    @Before
    fun setup() {
        presenter = ShopsMapPresenter(view, GetShopMarkersUseCase(repository), testScheduler, testScheduler)

    }

    @Test
    fun testNoShopMarkers() {
        given(repository.getAllShops()).willReturn(Single.just(emptyList()))
        presenter.getShopsMarkers()
        testScheduler.triggerActions()
        verify(view, never()).drawShopOnMap(anything())
    }

    @Test
    fun testThrowException() {
        val error = "Test error"
        val single: Single<List<Shop>> = Single.create { emitter ->
            emitter.onError(Exception(error))
        }
        `when`(repository.getAllShops()).thenReturn(single)

        presenter.getShopsMarkers()
        testScheduler.triggerActions()
        verify(view).showNoShopsError()
    }

    @Test
    fun testOneMarker() {
        val shopMock = Shop("","","",false,"90","80","","","","","","")
        val shops = listOf(shopMock)
        val single: Single<List<Shop>> = Single.create { emitter ->
            emitter.onSuccess(shops)
        }
        `when`(repository.getAllShops()).thenReturn(single)


        presenter.getShopsMarkers()
        testScheduler.triggerActions()
        verify(view).drawShopOnMap(anything())

    }
    @Test
    fun testOneMarkerWithInvalidPosition() {
        val shopMock = Shop("","","",false,"","","","","","","","")
        val shops = listOf(shopMock)
        val single: Single<List<Shop>> = Single.create { emitter ->
            emitter.onSuccess(shops)
        }
        `when`(repository.getAllShops()).thenReturn(single)


        presenter.getShopsMarkers()
        testScheduler.triggerActions()
        verify(view,never()).drawShopOnMap(anything())
        verify(view).showNoShopsError()

    }


}
