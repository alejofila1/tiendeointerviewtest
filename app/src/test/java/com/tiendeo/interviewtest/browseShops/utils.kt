package com.tiendeo.interviewtest.browseShops

import org.mockito.Mockito


fun <T> anything(): T {
    Mockito.any<T>()
    return uninitialized()
}

fun <T> uninitialized(): T = null as T