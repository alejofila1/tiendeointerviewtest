package com.tiendeo.interviewtest.browseShops.presenter

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import com.example.domain.usecases.GetShopsUseCase
import com.tiendeo.interviewtest.browseShops.anything
import com.tiendeo.interviewtest.common.ShopMapper
import com.tiendeo.interviewtest.common.uimodel.ShopUiModel
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.never
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class ShopListPresenterTest{

    @Mock
    lateinit var view :ShopListView

    @Mock
    lateinit var repository: ShopRepository

    private val testScheduler = TestScheduler()
    private lateinit var presenter: ShopListPresenter

    @Before
    fun setup(){
        presenter = ShopListPresenter(view, GetShopsUseCase(repository),testScheduler,testScheduler)

    }
    @Test
    fun testZeroShop(){
        val singleEmptyList = Single.just(emptyList<Shop>())
        Mockito.`when`(repository.getAllShops()).thenReturn(singleEmptyList)
        presenter.getShopList()
        testScheduler.triggerActions()
        Mockito.verify(view).showNoShopMessage()

    }


    @Test
    fun testOneShop(){
        val shopMock = Shop("","","",false,"","","","","","","","")
        val shops = listOf(shopMock)
        val singleOneShop = Single.just(shops)
        Mockito.`when`(repository.getAllShops()).thenReturn(singleOneShop)
        presenter.getShopList()
        testScheduler.triggerActions()
        Mockito.verify(view).showShopList(anything())
    }


}