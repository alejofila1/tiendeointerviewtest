package com.example.domain.usecases

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetShopMarkersUseCaseTest {

    @Mock
    lateinit var repository: ShopRepository
    lateinit var getShopMarkersUseCase: GetShopMarkersUseCase
    @Before
    fun setup() {
        getShopMarkersUseCase = GetShopMarkersUseCase(repository)
    }

    @Test
    fun invokeShopMarkerOneObject() {
        val shop = Shop(
                "abc", "abc", "abc",
                false,
                "60",
                "60",
                "",
                "",
                "",
                "",
                "",
                "")

        `when`(repository.getAllShops()).thenReturn(Single.just(listOf(shop)))
        val testObserver = getShopMarkersUseCase.invoke().test()
        testObserver.assertNoErrors()
        testObserver.assertValue { it ->
            it.shop == shop
                    && it.latLng.lat == shop.latitude.toDouble()
                    && it.latLng.long == shop.longitude.toDouble()
        }


    }

    fun invokeShopMarkerZeroObject() {
        `when`(repository.getAllShops()).thenReturn(Single.error(java.lang.Exception()))
        val testObserver = getShopMarkersUseCase().test()


    }


}