package com.example.domain.usecases

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetShopsUseCaseTest {

    @Mock
    lateinit var shopRepository: ShopRepository
    lateinit var getShopsUseCase : GetShopsUseCase

    @Before
    fun setUp() {
        getShopsUseCase = GetShopsUseCase(shopRepository)
    }

    @Test
    fun testGettingEmptyListOfShops() {
        Mockito.`when`(shopRepository.getAllShops()).thenReturn(Single.just(emptyList<Shop>()))
        val testObserver = getShopsUseCase().test()
        testObserver.assertValue{it.isEmpty()}

    }
    @Test
    fun testGettingListOfMultipleShops() {
        val shop = mock(Shop::class.java)
        val list = listOf(shop, shop,shop)
        Mockito.`when`(shopRepository.getAllShops()).thenReturn(Single.just(list))
        val testObserver = getShopsUseCase().test()
        testObserver.assertValue{it==list}

    }
}