package com.example.domain

import com.example.domain.model.Shop
import io.reactivex.Single

interface ShopRepository{
    fun getAllShops() : Single<List<Shop>>
    fun getShopById(shopId: String): Single<Shop>
}