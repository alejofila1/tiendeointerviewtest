package com.example.domain.usecases

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetShopsUseCase(private val shopsRepository: ShopRepository) {
    operator fun invoke (): Single<List<Shop>> {
        return shopsRepository.getAllShops()

    }
}