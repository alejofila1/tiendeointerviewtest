package com.example.domain.usecases


import com.example.domain.ShopRepository
import com.example.domain.model.LatLong
import com.example.domain.model.ShopLatLangPair
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

import java.lang.Float


open class GetShopMarkersUseCase(private val repository: ShopRepository){
    operator fun invoke() : Observable<ShopLatLangPair>{
        return repository.getAllShops()
                .flattenAsObservable { it }
                .map { shop ->
                    ShopLatLangPair(shop,
                            LatLong(Float.valueOf(shop.latitude.replace(",", ".")).toDouble()
                                    , Float.valueOf(shop.longitude.replace(",", ".")).toDouble()))
                }


    }
}