package com.example.domain.model

open class Shop(
        var addressCity: String,
        var addressPostalCode: String,
        var addressStreet: String,
        var isHasCatalogs: Boolean = false,
        var latitude: String,
        var longitude: String,
        var phoneNumber: String,
        var retailerId: String,
        var retailerName: String,
        var shopId: String,
        var shopName: String,
        var web: String)





