package com.example.domain.model




data class ShopLatLangPair(val shop: Shop, val latLng: LatLong)