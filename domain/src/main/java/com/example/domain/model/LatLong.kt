package com.example.domain.model

data class LatLong(val lat: Double, val long: Double)