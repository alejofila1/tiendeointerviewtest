package com.tiendeotest.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "shop")
class ShopEntity(
        @SerializedName("shop_id")
        @PrimaryKey
        @ColumnInfo(name = "shop_id")
        var shopId: String,

        @SerializedName("address_city")
        @ColumnInfo(name = "address_city")
        var addressCity: String,

        @SerializedName("address_postal_code")
        @ColumnInfo(name = "address_postal_code")
        var addressPostalCode: String,

        @SerializedName("address_street")
        @ColumnInfo(name = "address_street")
        var addressStreet: String,

        @SerializedName("has_catalogs")
        @ColumnInfo(name = "has_catalogs")
        var isHasCatalogs: Boolean = false,

        @SerializedName("latitude")
        @ColumnInfo(name = "latitude")
        var latitude: String,

        @SerializedName("longitude")
        @ColumnInfo(name = "longitude")
        var longitude: String,

        @SerializedName("phone_number")
        @ColumnInfo(name = "phone_number")
        var phoneNumber: String,

        @SerializedName("retailer_id")
        @ColumnInfo(name = "retailer_id")
        var retailerId: String,
        @SerializedName("retailer_name")
        @ColumnInfo(name = "retailer_name")

        var retailerName: String,
        @SerializedName("shop_name")
        @ColumnInfo(name = "shop_name")
        var shopName: String,

        @SerializedName("web")
        @ColumnInfo(name = "web")
        var web: String)

