package com.tiendeotest.data.repository

import com.example.domain.ShopRepository
import com.example.domain.model.Shop
import com.tiendeotest.data.api.ShopsApi
import com.tiendeotest.data.db.ShopDao
import com.tiendeotest.data.mapper.ShopEntityMapper
import com.tiendeotest.data.memory.ShopInMemory
import com.tiendeotest.data.utils.ConnectivityManagerWrapper
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ShopRepositoryImpl(
        val shopInMemory: ShopInMemory,
        val shopsApi: ShopsApi,
        val shopsDao: ShopDao,
        val connectivityWrapper: ConnectivityManagerWrapper) : ShopRepository {
    override fun getAllShops(): Single<List<Shop>> {

        if (shopInMemory.isEmpty()) {
            return if (connectivityWrapper.isConnectedToNetwork()) {
                shopsApi.getShops()
                        .doAfterSuccess { shopInMemory.saveShops(it) }
                        .doAfterSuccess { shopsDao.insertAll(it) }
                        .flattenAsObservable { it }
                        .map { ShopEntityMapper.fromEntityToDomain(it) }
                        .toList()
            } else {
                shopsDao.getAllShops()
                        .doAfterSuccess { shopInMemory.saveShops(it) }
                        .flattenAsObservable { it }
                        .map { ShopEntityMapper.fromEntityToDomain(it) }
                        .toList()
            }

        } else {
            return shopInMemory.getInMemoryShops()
                    .flattenAsObservable { it }
                    .map { ShopEntityMapper.fromEntityToDomain(it) }
                    .toList()
        }
    }

    override fun getShopById(shopId: String): Single<Shop> {
        return shopsDao.getShopById(shopId)
                .map { ShopEntityMapper.fromEntityToDomain(it) }
    }

}