package com.tiendeotest.data.utils

class NetworkData(val hasInternetConnection: Boolean, val isMobileConnection: Boolean)