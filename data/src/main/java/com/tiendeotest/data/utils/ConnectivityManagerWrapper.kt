package com.tiendeotest.data.utils

interface ConnectivityManagerWrapper {

    fun isConnectedToNetwork(): Boolean

    fun getNetworkData(): NetworkData
}