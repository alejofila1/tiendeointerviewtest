package com.tiendeotest.data.memory

import com.tiendeotest.data.model.ShopEntity
import io.reactivex.Single

class ShopInMemory {
    private val list: MutableList<ShopEntity>

    init {
        list = mutableListOf()
    }

    fun getInMemoryShops(): Single<List<ShopEntity>> {
        return Single.just(list)
    }

    fun isEmpty(): Boolean = list.isEmpty()

    fun saveShops(list: List<ShopEntity>) {
        this.list.addAll(list)
    }
}