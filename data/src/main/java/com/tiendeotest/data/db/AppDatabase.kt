package com.tiendeotest.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.tiendeotest.data.model.ShopEntity


@Database(entities = [ShopEntity::class],
        version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun shopDao(): ShopDao

    companion object {
        val DATABASE_NAME = "basic-sample-db"

        fun getInstance(context: Context): AppDatabase {

            return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DATABASE_NAME)
                    .build()

        }
    }
}


