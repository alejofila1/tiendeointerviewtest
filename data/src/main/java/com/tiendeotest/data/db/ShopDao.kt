package com.tiendeotest.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.domain.model.Shop
import com.tiendeotest.data.model.ShopEntity
import io.reactivex.Single



@Dao
interface ShopDao {
    @Query("SELECT * FROM shop")
    fun getAllShops(): Single<List<ShopEntity>>

    @Query("SELECT * FROM shop WHERE shop_id == :shop_id")
    fun getShopById(shop_id: String): Single<ShopEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg shops: ShopEntity)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(shop: List<ShopEntity>)
}