package com.tiendeotest.data.api

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ShopsService{
    val API_URL = "https://interview-test-45073.firebaseio.com/"

    fun getShopsApi() : ShopsApi{
        val retrofit = Retrofit.Builder().
                baseUrl(API_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        return retrofit.create(ShopsApi::class.java)
    }
}