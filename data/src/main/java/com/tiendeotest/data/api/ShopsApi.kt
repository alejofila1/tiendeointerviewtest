package com.tiendeotest.data.api

import com.tiendeotest.data.model.ShopEntity
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET

interface ShopsApi {
    @GET("shops.json")
    fun getShops() : Single<List<ShopEntity>>
}