package com.tiendeotest.data.mapper

import com.example.domain.model.Shop
import com.tiendeotest.data.model.ShopEntity

object ShopEntityMapper{
    fun fromEntityToDomain(shopEntity: ShopEntity) : Shop{
        with(shopEntity){
            return Shop(addressCity,
                    addressPostalCode,
                    addressStreet,
                    isHasCatalogs,
                    latitude,
                    longitude,
                    phoneNumber,
                    retailerId,
                    retailerName,
                    shopId,
                    shopName,
                    web)

        }
    }
}